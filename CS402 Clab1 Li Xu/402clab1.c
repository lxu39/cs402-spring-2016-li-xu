#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX 1024
#define MAXNAME  64     // #define and use constants for values that don't change

struct person {
    int id;
    char firstname[MAXNAME];
    char lastname[MAXNAME];
    double salary;
}employee[MAX];

int num = 0;

FILE * open_file(char* file){
    
    FILE * fp;
    
    fp = fopen(file, "r");
    
    if(fp == NULL)
    {
        printf("No such file named %s\n", file);
        return NULL;
    }
    else
        return fp;
}

int read_int(int * x, char * p){
    int res = atoi(p);
    
    if(x == NULL)
        return -1;
    else
    {
        *x = res;
        return 0;
    }
}

int read_float(double * f, char * p){
    double res = atof(p);
    
    if(f == NULL)
        return -1;
    else
    {
        *f = res;
        return 0;
    }
}

int read_string(char * s, char * p){
    
    strcpy(s,p);
    return 0;
}

int partition(struct person *data,int low,int high)
{
    struct person t;
    t = data[low];
    
    /******************************** Using for debugging *******************************
    printf("t: %d %s %s %lf\n",t.id,t.firstname,t.lastname,t.salary);
    printf("high: %d\n", high);
    printf("low: %d\n", low);
     */
    
    while(low < high)
    {
        while(low < high && data[high].id >= t.id)
            high--;
        
        data[low].id = data[high].id;
        data[low].salary = data[high].salary;
        if(low != high){
            strcpy(data[low].firstname,data[high].firstname);
            strcpy(data[low].lastname,data[high].lastname);
        }
        

        
        while(low < high && data[low].id <= t.id)
            low++;
        
        data[high].id = data[low].id;
        data[high].salary = data[low].salary;
        if(low != high){
            strcpy(data[high].firstname,data[low].firstname);
            strcpy(data[high].lastname,data[low].lastname);
        }
        
    }
    
    //printf("Check point\n");
    
    data[low].id = t.id;
    data[low].salary = t.salary;
    strcpy(data[low].firstname, t.firstname);
    strcpy(data[low].lastname, t.lastname);
    
    return low;
}

void sort(struct person *data,int low,int high)
{
    int pivotloc = 0;
    
    if(low >= high)
        return;
    
    pivotloc = partition(data,low,high);
    //printf("pivotloc: %d\n", pivotloc);
    sort(data,low,pivotloc-1);
    sort(data,pivotloc+1,high);
}

int start_up(){
    
    FILE *fp;
    char StrLine[1024];
    char *delim = " ";
    char *p;
    
    fp = open_file("small.txt");
    
    if(fp == NULL)
    {
        printf("Check point\n");
        return -1;
    }
    
    while (!feof(fp))
    {
        //fgets(StrLine,1024,fp);
        
        if(NULL == fgets(StrLine,1024,fp))
            continue;
        else{
            read_int(&employee[num].id, strtok(StrLine, delim));
            read_string(&employee[num].firstname, strtok(NULL, delim));
            read_string(&employee[num].lastname, strtok(NULL, delim));
            read_float(&employee[num].salary, strtok(NULL, delim));
            num++;
        }
    }
    fclose(fp);
    
    //printf("num: %d\n", num);
    sort(employee,0,num-1);
    
    return 0;
}

void printIns(){
    printf("\n");
    printf("Employee DB Menu:\n");
    printf("-----------------------------------------------------------------\n");
    printf("(1) Print the Database \n");
    printf("(2) Lookup employee by ID \n");
    printf("(3) Lookup employee by last name \n");
    printf("(4) Add an Employee \n");
    printf("(5) Quit \n");
    printf("-----------------------------------------------------------------\n");
}

int binarysearch(struct person * data, int search, int n)
{
    int low = 0, high = n-1;
    int mid;
    
    while (low <= high)
    {
        mid = (low + high)/2;
        
        if (data[mid].id == search)
        {
            return mid;
        }
        else if (data[mid].id > search)
        {
            high = mid - 1;
        }
        else if (data[mid].id < search)
        {
            low = mid + 1;
        }
    }
    
    return -1;
}

int main(){
    
    int i, command, id, searchRes,result;
    double salary;
    int foundlast = 0;
    char lastname[MAXNAME];
    char firstname[MAXNAME];
    
    start_up();
    
    
    /******************************* Using for debugging *******************************
     printf("%d\n",num);
     
     for(i=0;i<num;i++)
     {
     printf("%d %s %s %lf\n",employee[i].id,employee[i].firstname,employee[i].lastname,employee[i].salary);
     }
     */
    
    
    while(1){
        printIns();
        
        printf("\nYour command is: ");
        scanf("%d", &command);
        printf("\n");
        
        switch (command) {
            case 1:
                printf("NAME                                         SALARY        ID\n");
                printf("-----------------------------------------------------------------\n");
                for(i=0;i<num;i++)
                {
                    printf("%-15s         %-15s      %.0lf     %d\n",employee[i].firstname,employee[i].lastname,employee[i].salary,employee[i].id);
                }
                printf("-----------------------------------------------------------------\n");
                printf("Number of Employees (");
                printf("%d",num);
                printf(")");
                printf("\n");
                break;
                
            case 2:
                printf("Lookup employee by id \n");
                printf("Enter a 6 digit employee id: ");
                scanf("%d", &id);
                
                searchRes = binarysearch(employee,id,num);
                //printf("searchRes: %d\n",searchRes);
                if(searchRes == -1)
                {
                    printf("Employee with id ", id);
                    printf(" not found in DB");
                    printf("\n");
                }
                else
                {
                    printf("NAME                                         SALARY        ID\n");
                    printf("-----------------------------------------------------------------\n");
                    printf("%-15s         %-15s      %.0lf     %d\n",employee[searchRes].firstname,employee[searchRes].lastname,employee[searchRes].salary,employee[searchRes].id);
                    printf("-----------------------------------------------------------------\n");
                }
                break;
                
            case 3:
                printf("Lookup employee by lastname \n");
                printf("Enter Employee's last name (no extra spaces): ");
                scanf("%s", &lastname);
                for(i=0;i<num;i++)
                {
                    if(strcmp(employee[i].lastname, lastname) == 0){
                        printf("NAME                                         SALARY        ID\n");
                        printf("-----------------------------------------------------------------\n");
                        printf("%-15s         %-15s      %.0lf     %d\n",employee[i].firstname,employee[i].lastname,employee[i].salary,employee[i].id);
                        printf("-----------------------------------------------------------------\n");
                        foundlast++;
                    }
                    
                }
                if(foundlast == 0)
                    printf("There is no employee whose lastname is %s\n", lastname);
                foundlast = 0;
                printf("\n");
                break;
                
            case 4:
                while(1){
                    printf("\n");
                    printf("Add an Employee \n");
                    
                    printf("Enter the first name of the employee: ");
                    scanf("%s", &firstname);
                    printf("Enter the last name of the employee: ");
                    scanf("%s", &lastname);
                    
                    while(1){
                        printf("Enter a 6 digit employee id num: ");
                        scanf("%d", &id);
                        if(id >= 100000 && id <= 999999)
                            break;
                        printf("The six digit ID value must be between 100000 and 999999 inclusive, please give me another one.\n");
                    }
                    
                    while(1){
                        printf("Enter employee's salary (30000 to 150000): ");
                        scanf("%lf", &salary);
                        if(salary >= 30000 && salary <= 150000)
                            break;
                        printf("Salary amounts must be between $30000 and $150000 inclusive(no comma), please give me another one.\n");
                    }
                    
                    printf("Do you want to add the following employee to the DB?\n");
                    printf("    %s %s， salary: %.0lf, id: %d\n",firstname,lastname,salary,id);
                    printf("Enter 1 for yes, 0 for no: ");
                    scanf("%d",&result);
                    if (result == 1){
                        break;
                    }
                }
                
                
                
                employee[num].id = id;
                employee[num].salary = salary;
                strcpy(employee[num].firstname,firstname);
                strcpy(employee[num].lastname,lastname);
                num++;
                
                
                FILE *f = fopen("small.txt", "a");
                if (f == NULL)
                {
                    printf("Error opening file!\n");
                    exit(1);
                }
                    fprintf(f,"\n%d %s %s %.0lf",id,firstname,lastname,salary);
                
                fclose(f);
                
                
                
                sort(employee,0,num-1);
                
                printf("Employee Added\n");
                printf("\n");
                break;
                
            case 5:
                printf("Good Bye!\n");
                return 0;
                
            default:
                printf("Hey, %d is not between 1 and 5, try again...\n",command);
                break;
        }
    }
    
}