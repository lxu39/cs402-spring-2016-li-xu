
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

double mean(double * data, int size)
{
    int i;
    double sum = 0, mean;
    
    for(i = 0; i < size; i++)
    {
        sum = sum + data[i];
    }
    return mean = sum / size;
}

double median(double * data, int size)
{
    int i, j;
    double temp;
    
    for(i = 0; i < size - 1; i++)
    {
        for(j = i + 1; j < size; j++)
        {
            if(data[j] < data[i])
            {
                temp = data[i];
                data[i] = data[j];
                data[j] = temp;
            }
        }
    }
    
    if(size % 2 == 0)
    {
        return ((data[size / 2] + data[size / 2 - 1]) / 2);
    }
    else
        return data[size / 2];
    
}

double std_dev(double * data, int size)
{
    int i, j;
    double sum = 0, sum1 = 0, mean, variance, std_dev;
    
    for(i = 0; i < size; i++)
    {
        sum = sum + data[i];
    }
    mean = sum / size;
    for(j = 0; j < size; j++)
    {
        //printf("pow: %lf; data: %lf\n", pow((data[j] - mean), 2), data[j]);
        sum1 = sum1 + pow((data[j] - mean), 2);
    }
    variance = sum1 / size;
    //printf("size: %d; sum: %lf\n",size,sum1);
    return std_dev = sqrt(variance);
}

int main(int argc,char **argv){

    double * old, * new, * tmp;
    double dou;
    int size = 0, i;
    int returnTest;
    int currentSize = 20;
    FILE * fp;
    
    
    if(argc != 2)
    {
        printf("Please try again with the correct argument\n");
        return 0;
    }
     
    
    
    old = (double *)malloc(currentSize * sizeof(double));
    fp = fopen(argv[1], "r");
    if(fp == NULL)
        printf("No such file\n");
    
    while (!feof(fp)){
                
        if(size < currentSize)
        {
            fscanf(fp,"%lf",&dou);
            old[size++] = dou;
        }
        else
        {
            new = (double *) malloc(2 * currentSize * sizeof(double));
            for(i = 0; i < currentSize; i++)
                new[i] = old[i];
            currentSize = 2 * currentSize;
                
            tmp = old;
            old = new;
            new = tmp;
                
            free(new);
                
            fscanf(fp,"%lf",&old[size++]);
        }
    }
    
    /*
    for(i = 0; i < size; i++)
    {
     printf("%lf\n", old[i]);
    }
     */
     
    printf("Results:\n");
    printf("--------\n");
    printf("Num values:      ");
    printf("%8d\n",size);
    printf("      mean:      ");
    printf("%8.3lf\n",mean(old, size));
    printf("    median:      ");
    printf("%8.3lf\n",median(old, size));
    printf("    stddev:      ");
    printf("%8.3lf\n",std_dev(old, size));
    printf("Unused array capacity: %d\n", currentSize - size);

}