CS402 Clab2 Li Xu A20300818

In terminal, please use gcc command to compile

Then run it with one command line argument.

The command and output are as follows:

XULIs-MacBook-Pro:CS402clab2 LiXu$ gcc CS402clab2.c -o basicstats
XULIs-MacBook-Pro:CS402clab2 LiXu$ ./basicstats small.txt
Results:
--------
Num values:            12
      mean:        85.776
    median:        67.470
    stddev:        90.380
Unused array capacity: 8
XULIs-MacBook-Pro:CS402clab2 LiXu$ ./basicstats large.txt
Results:
--------
Num values:            30
      mean:      2035.600
    median:      1956.000
    stddev:      1496.153
Unused array capacity: 10
