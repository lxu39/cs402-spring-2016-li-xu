import java.util.Random;
public class MultiMatrix_double {
	static double[][] matrixProduct;
	static Random random=new Random();

	public static void main(String args[]){
		
		/*
		 * Set the numbers of row and column of matrix a,b
		 * length1 is rows of matrix a and columns of matrix b
		 * length2 is columns of matrix a
		 * length3 is rows of matrix b
		 * Matrix a is 400*300 and matrix b is 300*500
		 */
		int length1 = 300;
		int length2 = 400;
		int length3 = 500;
		double a[][] = new double[length2][length1];
		double b[][] = new double[length1][length3];
		
		/*
		 * Run 10 times of program to measure the average runtime and print it out
		 */
		long tmp=0;
		for(int i=0;i<10;i++){
			long start = System.currentTimeMillis();
			matrixGenerate(a,b);
			multiMatrix(a,b);
			long end = System.currentTimeMillis();
			System.out.println("RunTime: " + (end-start) + " ms");
			tmp+=(end-start);
		}
		System.out.println("Average Rumtime: "+tmp/10 + " ms");
 	}

	/*
	 * Randomly generate matrix a and b using real numbers (double)
	 */
	public static void matrixGenerate(double a[][],double b[][]){
		for(int i=0;i<a.length;i++){
			for(int j=0;j<a[0].length;j++){
				a[i][j]=random.nextDouble();
			}
		}
		for(int i=0;i<b.length;i++){
			for(int j=0;j<b[0].length;j++){
			b[i][j]=random.nextDouble();
			}
		}
	}

	/*
	 * Calculate the product of matrix a and b
	 */
	public static void multiMatrix(double a[][],double b[][]){
		matrixProduct=new double[a.length][b[0].length];//the product is a 400*500 matrix
			for (int i = 0; i<a.length; i++){
				for (int j = 0; j<b[0].length; j++){
					for (int k = 0; k<a[0].length; k++){
						matrixProduct[i][j]=matrixProduct[i][j]+a[i][k]*b[k][j];
					}
				}
			}
	}
}